// @flow
import React from 'react'
import imgError from 'images/404-error.svg'


export const ErrorPage = () => {
  return (
    <div className="error-page">
      <img src={imgError} />
    </div>
  )
}
