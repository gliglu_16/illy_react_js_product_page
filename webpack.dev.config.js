const path = require( 'path' )
const webpack = require( 'webpack' )
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack');


module.exports = {
  // devtool: 'cheap-module-eval-source-map',
  mode: 'development',
  // node: {
  //  fs: "empty"
  // },
  entry: [
    'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr&reload=true',
    './src/index.js'
  ],
  output: {
    path: path.resolve( __dirname, 'dist' ),
    filename: 'bundle.js',
    publicPath: '/',
  },
  plugins: [
    new Dotenv(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      title: 'Illy Product',
      hash: false,
      template: './templates/index.html'
    }),
    // new webpack.ProvidePlugin( {
    //   Cookies: 'js-cookie',
    // } )
  ],
  module: {
    rules: [
      // {
      //   enforce: 'pre',
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   use: [
      //     {
      //       loader: 'eslint-loader'
      //     }
      //   ]
      // },
      {
        test: /\.jsx?$/,
        include: [path.resolve( __dirname, 'src' )],
        exclude: [path.resolve( __dirname, 'src/*/tests/' )],
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-react']
            }
          }
        ]

      },
      {
        test: /\.styl$/,
        use: [
          'style-loader',
          'css-loader',
          'stylus-loader?resolve url'
        ],
        include: [path.join( __dirname, 'static/styles' )]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader'
        ]
      },
      {
        test: /\.(ttf|otf|woff|eot)$/,
        // loader: 'url-loader?importLoaders=1&limit=10000000',
        use: [
                {
                    loader: 'url-loader',
                    options: {
                        limit: 10000000,
                    }
                }
            ],
        include: path.join( __dirname, 'static/fonts' )
      },
      {
        test: /\.(jpg|jpeg|svg|png)$/,
        use: [
                {
                    loader: 'url-loader',
                    options: {
                        limit: 10000000,
                    }
                }
            ]
        // loader: 'url-loader?importLoaders=1&limit=10000000'
      }
    ]
  },
  resolve: {
    modules: [
      'node_modules',
      path.resolve( __dirname, 'static' ),
      path.resolve( __dirname, 'src/apps/' )
    ]
  }
};
