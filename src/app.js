import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Switch } from 'react-router-dom'
import history from './apps/utils/history'
import Favicon from 'react-favicon'
import faviconUrl from 'images/logo.png'

import Main from './apps/illy/containers/Main.jsx'


export const DefaultRouter = () => (
  <React.Fragment>
    <Favicon url={faviconUrl} />
    <Main />
  </React.Fragment>
)
//
// <React.Fragment>
//   <Favicon url={faviconUrl} />
//   <Router history={history}>
//     <Switch>
//       <Route path="/" component={Main} />
//     </Switch>
//   </Router>
// </React.Fragment>
