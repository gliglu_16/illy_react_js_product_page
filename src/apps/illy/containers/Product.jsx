import React, { useState, Suspense, lazy } from 'react'

import { IntlProvider, FormattedMessage } from 'react-intl'

import ukrainianMessages from '../../translations/uk.js'

import Carousel from "../components/Carousel.jsx"
import Quantity from "../components/Quantity.jsx"

const header_links = [
  ['COFFEE', '/'],
  ['COFFEE MACHINES', '/'],
  ['STORES', '/'],
]

const products = [
  {
    name: 'Capsule coffee machine - X1 Iperespresso Anniversary 1935',
    description: `
    <h3 class="text-lg font-medium">Preparations<h3>
    <p class="text-sm">Espresso coffee |  Drip coffee</p>
    <br>
    <h3 class="text-lg font-medium">Features</h3>
    <p class="text-sm">An icon of taste and design, the X1 machine originally created 20 years ago, is now available as a special edition multi-beverage machine. Prepare espresso, cappuccino, coffee, and latte with ease.</p>
    <br>
    <p class="text-sm">Product 60320</p>
    `,
    colors: ['#212121', '#C4C4C4', '#D8251B'],
    price: '£ 416.00'
  },
]

/**
 * The main container for the Checks that renders all verification steps.
 * @param {Object} props
 */
const Product = () => {
  return (
      <IntlProvider messages={ukrainianMessages} locale='uk' defaultLocale='en'>
          <div className="content">
            <div className="breadcrumps my-5 pl-[56px]">
            <nav className="bg-grey-light rounded-md w-full">
              <ol className="list-reset flex">
                <li><a href="#" className="text-gray-600 hover:text-gray-700">Coffee Machines</a></li>
                <li><span className="text-gray-500 mx-2">></span></li>
                <li><a href="#" className="text-gray-600 hover:text-gray-700">Capsule coffee machines for Iperespresso capsules - illy Shop</a></li>
                <li><span className="text-gray-500 mx-2">></span></li>
                <li className="text-red-500">Capsule coffee machine - X1 Iperespresso Anniversary 1935</li>
              </ol>
              </nav>
            </div>
            <div className="product grid grid-cols-2 pl-[56px]">
              <Carousel />
              <div className="description flex flex-col bg-neutral-200 pl-6 grow-ml-4">
                {products.map((product, index) => (
                  <div key={product.name}>
                    <div className="name py-4 text-xl font-bold">{product.name}</div>
                    <div dangerouslySetInnerHTML={{__html: product.description}} className="mb-8"></div>
                    <div className="color flex">
                      {product.colors.map((color, index) => (
                        <div key={color} style={{backgroundColor: color}} className="mr-4 w-5 h-5 border border-neutral-300 rounded-full"></div>
                      ))}
                    </div>
                    <div className="price flex justify-between">
                      <div className="price pr-24 my-5 text-3xl">{product.price}</div>
                      <div className="quantity flex"><Quantity /></div>
                      <div className="grow"></div>
                    </div>
                    <button className="button uppercase max-w-md mb-8 px-8 py-4 bg-red-500 text-white font-medium">Add to card</button>

                    <div className="grid grid-cols-2">
                      <div></div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
      </IntlProvider>
  )
}


export default Product
