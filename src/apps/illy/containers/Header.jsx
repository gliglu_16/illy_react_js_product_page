import React, { useState, Suspense, lazy } from 'react'

import { IntlProvider, FormattedMessage } from 'react-intl'

import ukrainianMessages from '../../translations/uk.js'
import iconUrl from 'images/logo.png'
import search from 'images/search.svg'
import profile from 'images/profile.svg'
import basket from 'images/basket.svg'


const header_links = [
  ['COFFEE', '/'],
  ['COFFEE MACHINES', '/'],
  ['STORES', '/'],
]

/**
 * The main container for the Checks that renders all verification steps.
 * @param {Object} props
 */
const Header = () => {
  return (
      <IntlProvider messages={ukrainianMessages} locale='uk' defaultLocale='en'>
        <nav className="flex flex-row justify-between mt-4 mx-[56px]">
          <div className='flex'>
          <div className='top-logo h-[67px] w-[67px] mr-4'>
            <img src={iconUrl} alt='Illy'></img>
          </div>
          <div className='menu mt-5 justify-self-start border-b border-b-neutral-500 mb-1 '>
            {header_links.map(([title, url], index) => (
              <a href={url}
                key={title}
                className={
                  `px-3 py-2 text-slate-700 font-medium hover:bg-slate-100 hover:text-slate-900 ${ index === 0 ? '' : 'border-l border-l-neutral-500' }`
                }>
                {title}
              </a>
            ))}
          </div>
        </div>
        <div className="grow border-b border-b-neutral-500 mb-1 mx-0 px-0"></div>
          <div className="navbar-menu flex justify-between justify-items-stretch mb-1 border-b border-b-neutral-500">
            <div className='right-menu flex justify-self-end mt-5'>
              <div className='mx-1.5 inline-block h-5 w-5'>
                <img src={search} alt='search'></img>
              </div>
              <div className='mx-1.5 inline-block h-5 w-5'>
                <img src={profile} alt='profile'></img>
              </div>
              <div className='mx-1.5 inline-block h-5 w-5'>
                <img src={basket} alt='basket'></img>
              </div>
            </div>
          </div>
        </nav>

      </IntlProvider>
  )
}


export default Header
