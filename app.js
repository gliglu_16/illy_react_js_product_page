var express = require( 'express' )
var path = require( 'path' )
var proxy = require( 'express-http-proxy' )


var app = express()

// Constants
const PORT = 3000
const HOST = '0.0.0.0'


app.use( express.static( path.join( __dirname, 'dist' ) ) )
app.use( express.static( path.join( __dirname, 'static' ) ) )
// respond with "hello world" when a GET request is made to the homepage

app.get('*', function(req, res) {
  res.sendFile( path.resolve( __dirname, 'dist/index.html' ) );
});

app.listen( PORT, HOST )
console.log( `Running on http://${ HOST }:${ PORT }` )
