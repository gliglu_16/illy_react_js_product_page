import React, { useState } from 'react'


  const Quantity = () => {

    const [value, setValue] = useState(1);

    const increment = () => (
      setValue(value < 100 ? value+1 : 0)
    )

    const decrement = () => (
      setValue(value > 0 ? value-1 : 0)
    )

    return (
        <div className="quantity-input">
          <button className="quantity-input__modifier quantity-input__modifier--left" onClick={decrement}>
            &mdash;
          </button>
          <input className="quantity-input__screen" type="text" value={value} readOnly />
          <button className="quantity-input__modifier quantity-input__modifier--right" onClick={increment}>
            &#xff0b;
          </button>
        </div>
    );
  }

export default Quantity
