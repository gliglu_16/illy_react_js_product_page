import React, { useState, Suspense, lazy } from 'react'

import { IntlProvider, FormattedMessage } from 'react-intl'

import ukrainianMessages from '../../translations/uk.js'

import product2 from 'images/product2.png'
import product3 from 'images/product3.png'
import product4 from 'images/product4.png'
import product5 from 'images/product5.png'


const recommendedProducts = [
  {
    name: 'X7.1 Iperespresso - Capsules Coffee Machine',
    colors: ['#212121', '#FFFFFF', '#D8251B'],
    price: '£ 156.00',
    photo: product2
  },
  {
    name: 'Y3.3 Espresso & Coffee Machine',
    colors: ['#749FAF', '#C4C4C4', '#D8251B'],
    price: '£ 110.00',
    photo: product3
  },
  {
    name: 'X9 Iperespresso - Capsules Coffee Machine',
    colors: ['#6F6F6F'],
    price: '£ 186.00',
    photo: product4
  },
  {
    name: 'Capsule coffee machine - X1 Iperespresso Anniversary 1935',
    colors: ['#D2CDBC'],
    price: '£ 416.00',
    photo: product5
  },
]

/**
 * The main container for the Checks that renders all verification steps.
 * @param {Object} props
 */
const Recommendations = () => {
  return (
      <IntlProvider messages={ukrainianMessages} locale='uk' defaultLocale='en'>
        <div className="content text-center py-8">
          <h2 className="text-lg uppercase my-8">Recommendations</h2>
          <div className="slider flex flex-row gap-x-12 justify-center">

          {recommendedProducts.map((product, index) => (
            <div key={product.name} className="flex flex-col w-[224px]">
            <div className="flex border border-neutral-300 w-[224px] h-[224px] justify-center ">
              <img src={product.photo} className="" alt="coffee machine"></img>
              </div>
              <div className="color flex justify-center">
                {product.colors.map((color, index) => (
                  <div key={color} style={{backgroundColor: color}} className="mr-4 my-1 w-5 h-5 border border-neutral-300 rounded-full"></div>
                ))}
              </div>
              <div className="name text-sm px-2">{product.name}</div>
              <div className="price">{product.price}</div>
            </div>

          ))}

          </div>
          </div>
      </IntlProvider>
  )
}


export default Recommendations
