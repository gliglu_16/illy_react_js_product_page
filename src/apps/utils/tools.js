// @flow

export const isEmpty = (obj: Object) => {
  return Object.keys(obj).length === 0
}

export const formPhotoUrl = url => {
  let newUrl = ''

  if (url) {
    newUrl = url.replace('localhost', 'localhost:8000')
  }
  return newUrl
}
