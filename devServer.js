const express = require( 'express' );
const path = require( 'path' );
const webpack = require( 'webpack' );
const config = require( './webpack.dev.config' );
const webpackDevMiddleware = require( 'webpack-dev-middleware' );
const proxy = require( 'express-http-proxy' );

const app = express();
const compiler = webpack( config );

app.use( webpackDevMiddleware( compiler, {
  // noInfo: true,
  publicPath: config.output.publicPath
} ) );

app.use( require( 'webpack-hot-middleware' )( compiler ) );

app.use( '/api', proxy( 'http://web:9000', {
  proxyReqPathResolver: function( req ) {
    return '/api'.concat(require( 'url' ).parse( req.url ).path);
  }
} ) );

app.get( '*', function( req, res ) {
  res.sendFile( path.join( __dirname, 'templates/index.html' ) );
} );


app.listen( 3000, '0.0.0.0', function( err ) {
  if ( err ) {
    console.error( err );
    return;
  }

  console.log( 'Listening at http://localhost:3000' );
});
