// @flow
import moment from 'moment'


export const convertDateFromISO = (data: Object, key: string) => {
  if (data instanceof Array) {
    return data.map(item => ({
      ...item,
      ...{ [key]: moment(item[key]).format('YYYY-MM-DD') }
    }))
  }
  if (data instanceof Object) {
    return {
      ...data,
      ...{ [key]: moment(data[key]).format('YYYY-MM-DD') }
    }
  }
}
