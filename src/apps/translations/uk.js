const ukrainianMessages = {
  makeSelfie: 'Зробити селфі',
  makeSelfieAgain: 'Зробити селфі знову',
  makeDocumentsPhoto: 'Зробити фото закордонного паспорту',
  makePassInternalPhoto: 'Зробити фото внутрішнього паспорту',
  makeIDCardFrontPhoto: 'Зробити фото фронтальної сторони ID картки',
  makeIDCardBackPhoto: 'Зробити фото зворотньої сторони ID картки',
  makeDriverLicenceFrontPhoto: 'Зробити фото водійських прав',
  makeDriverLicenceBackPhoto: 'Зробити фото зворотньої сторони водійських прав',
  makeSelfieWithDocuments: 'Зробити селфі з паспортом',
  stopVideoVerification: 'Завершити відеодзвінок',
  prepareForSelfie: 'Зараз треба буде зробити селфі,тож причепуріться трохи!',
  startDemo: 'Почати демо',
  goToSelfie: 'Ок, поїхали!',
  successUpload: '✅ Дякуємо, фото завантажено!',
  continue: 'Продовжити',
  retake: 'Перезняти',
  uploadPhoto: 'Завантажити фото',
  takePicture: 'Зробити фото',
  uploadPassHelpText: 'Завантажте будь ласка фото першого розвороту вашого паспорту',
  uploadIdCard1HelpText: 'Завантажте будь ласка фото фронтальної сторони вашої ID картки',
  uploadIdCard2HelpText: 'Завантажте будь ласка фото задньої сторони вашої ID картки',
  uploadIdSelfiePassHelpText: 'Зараз треба буде зробити селфі з паспортом, тож приготуйтесь!',
  signinInstructions: 'Будь ласка увійдіть щоб переглянути наявні верифікації',
  signin: 'Увійти',
  noOngoingVerefications: 'Немає користувачів для верифікацій'
}

export default ukrainianMessages
