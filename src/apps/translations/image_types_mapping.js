const imageTypesMapping = {
  SELFIE: 'Селфі',
  PASSPORT: 'Фото закордонного паспорту',
  SELFIE_WITH_PASSPORT: 'селфі з паспортом',
  IDCARD_FRONT: 'Фронтальна сторона ID картки',
  IDCARD_BACK: 'Зворотня сторона ID картки',
  DRIVER_LICENCE_FRONT: 'Фронтальна сторона водійських прав',
  DRIVER_LICENCE_BACK: 'Зворотня сторона водійських прав',
  PASSPORT_INTERNAL: 'Фото внутрішнього паспорта'
}

export default imageTypesMapping
