React.js client app for Illy Product.

To develop locally run
```bash
yarn install
yarn run start
```

To build production run
```bash
yarn build
```

Refer to package.json scripts section for all available options.
