import React from 'react'
import ReactDOM from 'react-dom'

import 'styles/index.css'
import { DefaultRouter } from './app'


const render = (Component) => {
  ReactDOM.render(
    <Component/>,
    document.getElementById('root'),
  )
}
//
render(DefaultRouter)
//
// if (process.env.ENV === 'development') {
  // Hot Module Replacement API
  // module.hot.accept('./index', () => {
  //   render(DefaultRouter)
  // })
// }
