FROM node:17-alpine

WORKDIR /srv/client

COPY ./package*.json ./
COPY ./yarn.lock ./

RUN yarn install
RUN yarn run install_webpack_globally

COPY . .
