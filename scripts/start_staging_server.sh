#!/bin/sh

rm -rf ./dist
mkdir ./dist

mkdir -p logs
yarn run build
yarn run staging
