import React, { useState, Suspense, lazy } from 'react'

import { IntlProvider, FormattedMessage } from 'react-intl'

import ukrainianMessages from '../../translations/uk.js'

import Product from './Product.jsx'
import Recommendations from './Recommendations.jsx'


/**
 * The main container for the Checks that renders all verification steps.
 * @param {Object} props
 */
const Content = () => {
  return (
      <IntlProvider messages={ukrainianMessages} locale='uk' defaultLocale='en'>
          <div className="mb-auto">
            <Product />
            <Recommendations />
          </div>

      </IntlProvider>
  )
}


export default Content
