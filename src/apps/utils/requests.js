// @flow

type Headers = {
  Authorization: string
}

type Args = {
  url: string,
  body: Object,
  headers: Headers,
  method: 'POST' | 'PUT' | 'PATCH' | 'DELETE'
}

export function ResponseError(message: string, body: Object) {
  // message: message of the response exception
  // body: object (array) of the error.
  this.message = message
  this.body = body
  this.name = 'ResponseError'
}

export const getHost = () => {
  // get current host

  return process.env.API_HOST
}

export const standardHeaders = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

export function isJson(response: any) {
  const contentType = response.headers.get('content-type')

  return contentType && contentType.indexOf('application/json') !== -1
}

export function checkStatus(response: any) {
  if (response.status >= 200 && response.status < 300) {
    return isJson(response) ?
      response.json()
        .then(json => json, () => response) // return response if no content or can't parse json;
      :
      response
  }

  let error

  if (response.status < 500) {
    error = response.json()
  } else {
    error = response.text()
  }

  return error.then(e => {
    throw new ResponseError(response.statusText, e)
  })
}

export function raiseError(e: Object) {
  const message = e.body || 'Oops! Something\'s getting wrong'

  throw new ResponseError(message, e.body)
}

export const encodeUrl = (params: Object) => {
  return '?'.concat(
    encodeURI(Object.keys(params).map(
      name => `${name}=${params[name]}`,
    ).join('&')),
  )
}

export function getUrlWithParams(
  url: string,
  method: string,
  body: Object | null = null, // why default value null?
) {
  if (body && method === 'GET') {
    // GET params into uri
    return url.concat(encodeUrl(body))
  }
  // POST etc
  return url

  // TODO: form data into uri
}

// url: string, data: object = {}, method: string, headers: object
export const send = (args: Args, isJsonBody: boolean = true) => {
  const {
    url,
    method,
    headers = standardHeaders,
    body = {}
  } = args
  const token = localStorage.getItem('token')

  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  let options = {
    credentials: 'same-origin',
    method,
    headers
  }

  if (['POST', 'PATCH', 'PUT'].includes(method)) {
    options = Object.assign({}, options, {
      body: isJsonBody ? JSON.stringify(body) : body
    })
  }
  const preparedUrl = getHost().concat(getUrlWithParams(url, method, body))

  // condition for pushed parameters to url in browser
  const returnData = fetch(preparedUrl, options)
    .then(checkStatus)
    .catch(raiseError)

  console.log(token)
  return returnData
}

export const decodeQueryParamsToObject = (query: string) =>
  query
    .slice(1)
    .split('&')
    .map(item => item.split('='))
    .filter(pair => pair.length === 2)
    .reduce((prev, [key, value]) => ({
      ...prev,
      [key]: decodeURI(value)
    }), {})
