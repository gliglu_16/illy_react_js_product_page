import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'


const styles = {
  loaderStyles: {
    display: 'inline-block',
    position: 'relative'
  }
}


const Loader = (props) => (
  <div
    style={props.styles}
    className={
      `loader ${props.positionClass ? props.positionClass : 'fixed'}` +
      ` ${props.bgColorClass ? props.bgColorClass : 'bg-transparent'}`
    }
  >
    <CircularProgress
      size={props.size || 50}
      top={0}
      left={0}
      status={'loading'}
      style={styles.loaderStyles}
    />
  </div>
)

export default Loader
