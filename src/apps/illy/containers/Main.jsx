import React, { useState, lazy } from 'react'

import { IntlProvider, FormattedMessage } from 'react-intl'

import ukrainianMessages from '../../translations/uk.js'
import iconUrl from 'images/logo.png'

import Header from './Header.jsx'
import Content from './Content.jsx'
import Footer from './Footer.jsx'

/**
 * The main container for the Checks that renders all verification steps.
 * @param {Object} props
 */
const Main = () => {
  return (
      <IntlProvider messages={ukrainianMessages} locale='uk' defaultLocale='en'>
        <div className="flex flex-col h-screen justify-between">
          <Header />
          <Content />
          <Footer />
        </div>
      </IntlProvider>
  )
}

export default Main
