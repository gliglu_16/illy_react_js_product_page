import React, { useState, Suspense, lazy } from 'react'

import { IntlProvider, FormattedMessage } from 'react-intl'

import ukrainianMessages from '../../translations/uk.js'
import iconUrl from 'images/logo.png'
import youtube from 'images/youtube.svg'
import facebook from 'images/facebook.svg'
import linkedin from 'images/linkedin.svg'
import instagram from 'images/instagram.svg'


const footer_links = [
  ['About us', '/'],
  ['Contact', '/'],
  ['Our story', '/'],
]

/**
 * The main container for the Checks that renders all verification steps.
 * @param {Object} props
 */
const Footer = () => {
  return (
      <IntlProvider messages={ukrainianMessages} locale='uk' defaultLocale='en'>
        <footer className="bottom-0 text-center flex flex-col justify-center justify-items-center content-center items-center">
          <div className="flex">
            <div className='menu mt-5 justify-self-start mb-1'>
              {footer_links.map(([title, url], index) => (
                <a href={url}
                  key={title}
                  className={
                    `px-6 py-2 text-neutral-700 font-normal hover:bg-slate-100 hover:text-slate-900 ${ index === 0 ? '' : 'border-l border-l-neutral-500' }`
                  }>
                  {title}
                </a>
              ))}
            </div>
          </div>
          <div className="container flex flex-row justify-center pt-6 p-2 mb-9">
            <a href="#!" className="mr-5 mx-1.5 inline-block">
              <img src={facebook} alt='facebook' className="h-5 w-5 pb-0.5"></img>
            </a>
            <a href="#!" className="mr-5 inline-block">
              <img src={youtube} alt='youtube' className="h-6 w-6 pb-1"></img>
            </a>
            <a href="#!" className="mr-5 inline-block">
              <img src={instagram} alt='instagram' className="h-5 w-5"></img>
            </a>
            <a href="#!" className="mr-5 text-gray-800 inline-block">
              <img src={linkedin} alt='linkedin' className="h-5 w-5 pb-0.5"></img>
            </a>

          </div>

      </footer>


      </IntlProvider>
  )
}

export default Footer
