const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const WorkboxPlugin = require( 'workbox-webpack-plugin' )
const Dotenv = require('dotenv-webpack');
const WebpackPwaManifest = require( 'webpack-pwa-manifest' )
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'production',
  entry: [
    './src/index'
  ],
  output: {
    filename: '[name].[chunkhash].js',
    path: path.resolve( __dirname, 'dist' )
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        include: [path.resolve( __dirname, 'src' )],
        exclude: [path.resolve( __dirname, 'src/*/tests/' )],
        loader: 'babel-loader',
        sideEffects: false,
        options: {
          presets: ['@babel/preset-react']
        }
      },
      {
        test: /\.css$/i,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader'
        ]
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'url-loader'],
      },
      {
        test: /\.png$/,
        use: ['url-loader'],
      },
    ]
  },
  plugins: [
    new Dotenv(),
    new BrotliPlugin({
      asset: '[path].br[query]',
      test: /\.(js|jsx|css|html|svg)$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new webpack.ProvidePlugin({
      Cookies: 'js-cookie',
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
    new HtmlWebpackPlugin({
      title: 'Illy Product',
      hash: true,
      template: './templates/index_prod.html'
    }),
    new WebpackPwaManifest( {
      name: 'Illy Product',
      short_name: 'Illy Product',
      description: 'Illy Product.',
      background_color: '#0e2d3f',
      display: 'standalone',
      crossorigin: 'use-credentials',
      theme_color: '#0e2d3f',
      ios: true,
      icons: [
        {
          src: path.resolve( 'static/images/logo.png' ),
          sizes: [128]
        },
        {
          src: path.resolve( 'static/images/logo.png' ),
          sizes: [192],
          ios: true,
        },
        {
          src: path.resolve( 'static/images/logo.png' ),
          sizes: [32]
        },
        {
          src: path.resolve( 'static/images/logo.png' ),
          sizes: [512]
        },
        {
          src: path.resolve( 'static/images/logo.png' ),
          sizes: [96, 256, 384]
        },
      ]
    } ),
    new CleanWebpackPlugin(),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all'
        },
        styles: {
          name: 'styles',
          test: /\.(css)$/,
          minChunks: 1,
          chunks: 'all',
          reuseExistingChunk: true,
          enforce: true
        }
      }
    },
    minimizer: [
      new OptimizeCSSAssetsPlugin({}),
      new UglifyJSPlugin({
        uglifyOptions: {
          sourceMap: false,
          compress: {
            drop_console: true,
            conditionals: true,
            unused: true,
            comparisons: true,
            dead_code: true,
            if_return: true,
            join_vars: true,
          },
          output: {
            comments: false
          }
        }
      })
    ],
  },
  resolve: {
    modules: [
      path.resolve( __dirname, 'src/apps/' ),
      path.resolve( __dirname, 'src' ),
      path.resolve( __dirname, 'static' ),
      'node_modules',
    ]
  }
}
